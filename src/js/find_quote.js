// Load stock graph with data of selected stock
function loadQuoteData(symbol, callback) {
  const url = "https://api.iextrading.com/1.0/stock/" + symbol + "/quote";
  var request = new XMLHttpRequest();

  request.open('GET', url, true);
  request.onload = function () {

    // Begin accessing JSON data here
    data = JSON.parse(this.response)

    if (request.status >= 200 && request.status < 400) {
      callback(null, data);
    } else {
      console.log('error');
    }
  }

  request.send();
}


function addToQuotes(id) {
  console.log(id);
  loadQuoteData(id.split(".")[0], function(error, response) {
    var newQuote = `
          <button class="quote updateable-quote" id="${response.symbol}" onclick="updateUiTOSelectedQuote(${id.split(".")[0]})">

            <div class="quote-block">
              <p id="ticker" class="block-info">${response.symbol}</p>
            </div>

            <div class="quote-block">
              <p id="price" class="block-info">${response.latestPrice}</p>
            </div>

            <div class="quote-block">
              <p id="open-close" class="block-info">${response.open}</p>
            </div>

            <div class="quote-block">
              <p id="open-close" class="block-info">${(response.latestSource === 'Close') ? 'Closed' : 'Open'}</p>
            </div>

            <div class="quote-block">
              <p id="change" class="block-info">${response.changePercent}</p>
            </div>
          </button>
      `;

    var quotesBoard = document.querySelector(".watchlist");
    quotesBoard.innerHTML += newQuote;
  });
}

function getSimilarQuote(keyword, callback) {
  const url = "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" + keyword + "&apikey=3BXRR7PJ2AX1AMUN";
  var request = new XMLHttpRequest();

  request.open('GET', url, true);
  request.onload = function () {

    // Begin accessing JSON data here
    var data = JSON.parse(this.response);

    if (request.status >= 200 && request.status < 400) {
      callback(null, data);
    } else {
      console.log('error');
    }
  }

  request.send();
}

function fillQuotesToSugestion(data) {
  var parent = document.querySelector(".similar-quotes");

  if (parent.childElementCount != 0) {
    while(parent.firstChild) {
      parent.removeChild(parent.firstChild)
    }
  }

  for (var i = 0; i < data.length; i++) {
    var newQuote = `
      <div class="quote search-quote" id="${data[i]['1. symbol']}" class="searchQuote-${data[i]['1. symbol']}">
        <p id="symbol">${data[i]['1. symbol']}</p>
        <p id="name">${data[i]["2. name"]}</p>
      </div>
    `

    parent.innerHTML += newQuote;
  }

  const elements = document.getElementsByClassName("search-quote");
  for (var i = 0; i < elements.length; i++) {
    const element = elements[i];
    element.addEventListener('click', function() {
      // Get the symbol of the element
      var symbol = element.id;
      addToQuotes(symbol);
      saveQuoteToDB(symbol);
    })
  }
}


var searchButton = document.querySelector(".search");
var searchSymbolBox = document.getElementById("stock-symbol");

searchButton.addEventListener('click', function() {
  var keyword = searchSymbolBox.value;
  getSimilarQuote(keyword, function(error, data) {
    if (!error) {
      console.log(data);
      fillQuotesToSugestion(data.bestMatches);
    }
  });
})
