const socket = io('https://ws-api.iextrading.com/1.0/tops');
//const socket_internal = io('http://0.0.0.0:8080');

function updateQuote(res) {
  var quoteToUpdate = document.getElementById(res['symbol']);

  var price = quoteToUpdate.querySelectorAll("#price");

  var openClose = quoteToUpdate.querySelectorAll("#open-close");
  var performance_nn = quoteToUpdate.querySelectorAll("#performance");
  var change = quoteToUpdate.querySelectorAll("#change");


  // Is it a trading day?
  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
  var firstDate = new Date(1970,01,01);
  var secondDate = new Date();

  var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
  var diffDaysSinceUpdate = res.lastUpdated;


  var isTrading = (diffDays - diffDaysSinceUpdate >= 0 ? false : true);

  if (!isTrading) {
      console.log("Market is closed!");
  }

  price[0].innerHTML = (isTrading ? res.askPrice : res.lastSalePrice);
  openClose[0].innerHTML = (isTrading ? res.bidPrice : res.lastSalePrice);
  change[0].innerHTML = "Null";
}

// Add stock quote to the UI
// Get current quotes in watchlist
var dataAddress = userPersistanceData.child("watchlist");
// Check if there is data in the db
dataAddress.once('value').then(function(snap) {
  console.log(snap.val());

  // If not create and add a new array
  if (snap.val() != null) {
    var symbolsArray = snap.val().symbols;
    for (var i = 0; i < symbolsArray.length; i++) {
      addToQuotes(symbolsArray[i]);
    }

    if (symbolsArray.length >= 3) {
      const buttonAddMore = document.querySelector(".button-wrapper");
      buttonAddMore.style.display = "none";
    } else {
      const buttonAddMore = document.querySelector(".button-wrapper");
      buttonAddMore.style.display = "block";
    }
  }
})
