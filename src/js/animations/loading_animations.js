var baseURL = "http://35.204.175.251/the-unity-trader/src/";

// Animation triggers
var startAiAgent = document.getElementById("start-ai-agent");

function revealSite() {
  var slidingOverlayOpen = anime({
    targets: '#sliding-overlay',
    translateX: "100%",
    easing: 'easeInOutQuart'
  });
}

function coverSite(destinationUrl) {
  var slidingOverlayClose = anime({
    targets: '#sliding-overlay',
    translateX: "0",
    easing: 'easeInOutQuart',
    complete: function(anim) {
      window.location.replace(baseURL + destinationUrl)
    }
  });
}


revealSite()
