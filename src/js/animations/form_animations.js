var frameworkButton = document.getElementById("framework");
var goalButton = document.getElementById("goal");
var payButton = document.getElementById("pay");

var nextButton = document.getElementById("next-section");


// Returns a value for the translateX css property
function getTranslateX(element) {
  var style = window.getComputedStyle(element);
  var matrix = new WebKitCSSMatrix(style.webkitTransform);

  return(matrix.m41)
}


function changeActiveUnderLine(x) {
  var slidingOverlayClose = anime({
    targets: '#sliding-overlay',
    translateX: x + "%",
    easing: 'easeInOutQuart',
  });
}

function updateSectionPlacement(id, element, translateXValue, translateXValueForm) {
  var slidingOverlayClose = anime({
    targets: '.active-underline',
    translateX: translateXValue + "px",
    easing: 'easeInOutQuart'
  });

  if (!element.classList.contains("active")) {
    element.classList.add("active");
  }

  var otherActive = document.querySelector(".active:not(#" + id + ")");
  otherActive.classList.remove("active");

  var slidingInNewForm = anime({
    targets: '.forms',
    translateX: -translateXValueForm + "vw",
    easing: 'easeInOutQuart'
  });
}

var formHeaderWidth = document.getElementsByClassName("form-header")[0].offsetWidth;

frameworkButton.addEventListener("click", function() {
  updateSectionPlacement("framework", frameworkButton, 0, 0);
});

goalButton.addEventListener("click", function() {
  updateSectionPlacement("goal", goalButton, 1/3 * formHeaderWidth, 100);
});

payButton.addEventListener("click", function() {
  updateSectionPlacement("pay", payButton, 2/3 * formHeaderWidth, 200);
});

nextButton.addEventListener("click", function() {
  var active = document.getElementsByClassName("active")[0];
  if (active.id == "framework") {
    updateSectionPlacement(active.id, frameworkButton, 0, 0);
  } else if (active.id == "goal"){
    updateSectionPlacement(active.id, goalButton, 1/3 * formHeaderWidth, 100);
  } else {
    updateSectionPlacement(active.id, payButton, 2/3 * formHeaderWidth, 200);
  }
});
