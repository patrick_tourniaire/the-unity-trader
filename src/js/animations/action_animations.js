var enableAddStockButton = document.getElementsByClassName("add-stock-button");
var enableAiAgentButtons = document.getElementsByClassName("start-agent-button");
var background = document.getElementById("toned-out-background");

function enableAddStock() {
  // Blur background
  background.style.display = "block";
  background.style.opacity = ".8";

  // Move search box down
  anime({
    targets: '.search-stock',
    translateY: 250,
    easing: 'easeInOutQuart',
    duration: 400
  });

  anime({
    targets: '.search-stock .similar-quotes',
    scale: {
      value: 1,
      duration: 300,
      easing: 'easeInOutQuart'
    }
  });
}

function disableAddStock() {
  // Blur background
  background.style.display = "none";
  background.style.opacity = "0";

  // Move search box down
  anime({
    targets: '.search-stock',
    translateY: -150,
    easing: 'easeInOutQuart',
    duration: 400
  });

  anime({
    targets: '.search-stock .similar-quotes',
    scale: {
      value: 0,
      duration: 300,
      easing: 'easeInOutQuart'
    }
  });
}

function enableAiAgent() {
  // Blur background
  background.style.display = "block";
  background.style.opacity = ".8";

  // Move search box down
  anime({
    targets: '.start-agent',
    translateY: 600,
    easing: 'easeInOutQuart',
    duration: 400
  });

  anime({
    targets: '.start-agent .similar-quotes',
    scale: {
      value: 1,
      duration: 300,
      easing: 'easeInOutQuart'
    }
  });
}

function disableAiAgent() {
  // Blur background
  background.style.display = "none";
  background.style.opacity = "0";

  // Move search box down
  anime({
    targets: '.start-agent',
    translateY: -500,
    easing: 'easeInOutQuart',
    duration: 400
  });

  anime({
    targets: '.start-agent .similar-quotes',
    scale: {
      value: 0,
      duration: 300,
      easing: 'easeInOutQuart'
    }
  });
}

var i = 0;
while (enableAddStockButton[i]) {
  enableAddStockButton[i].addEventListener("click", function() {
    enableAddStock();
  });
  i++;
}

for (var i = 0; i < enableAiAgentButtons.length; i++) {
  enableAiAgentButtons[i].addEventListener("click", function() {
    enableAiAgent();
    console.log("click");
  })
}

background.addEventListener("click", function() {
  disableAddStock();
  disableAiAgent();
})


// Action when selecting stock from quotes-board
function getCompanyInformation(symbol, callback) {
  const url = "https://api.iextrading.com/1.0/stock/" + symbol + "/company";
  var request = new XMLHttpRequest();

  request.open('GET', url, true);
  request.onload = function () {

    // Begin accessing JSON data here
    var data = JSON.parse(this.response);

    if (request.status >= 200 && request.status < 400) {
      callback(null, data);
    } else {
      console.log('error');
    }
  }

  request.send();
}
