
const userIdTesting = "334gfbbge4556";
const userPersistanceData = firebase.database().ref('users/' + userIdTesting + "/board");

// Save new quote to board
function saveQuoteToDB(symbol) {
  var dataAddress = userPersistanceData.child("watchlist");
  // Check if there is data in the db
  dataAddress.once('value').then(function(snap) {
    console.log(snap.val());

    // If not create and add a new array
    if (snap.val() == null) {
      var newArray = [symbol];
      dataAddress.set({
        symbols: newArray
      })
    } else if (!snap.val().symbols.includes(symbol)) {
      var symbolsArray = snap.val().symbols;
      symbolsArray.push(symbol);

      dataAddress.set({
        symbols: symbolsArray
      })
    }
  })
}
