
var nnHeader = document.querySelector(".trading-stats .trader-ticker");
var windows = document.getElementById("window");
var episodes = document.getElementById("episode");

var startAgentButton = document.getElementById("next-section");
console.log(startAgentButton);

// Listen for on click to start agent
startAgentButton.addEventListener("click", function() {
  console.log('click');

  var selectedSymbol = nnHeader.innerHTML;
  var numEpisodes = parseInt(episodes.value);
  var numWindows = parseInt(windows.value);

  // Send request to rest server
  if (numWindows != null && numEpisodes != null) {
    var request = new XMLHttpRequest();
    request.open("POST", "http://localhost:5005/start_agent?symbol=" + selectedSymbol + "&windows=" + numWindows + "&episodes=" + numEpisodes);

    request.onload = function () {

      // Begin accessing JSON data here
      data = JSON.parse(this.response)

      if (request.status >= 200 && request.status < 400) {
        callback(null, data);
      } else {
        console.log('error');
      }
    }

    request.send();
  }
});
