function buildChart(chartData) {
  var chart = AmCharts.makeChart("stock-live-graph", {

      type: "stock",

      categoryAxesSettings: {
        minPeriod: "mm"
      },

      dataSets: [{
        color: "#000",
        fieldMappings: [{
          fromField: "value",
          toField: "value"
        }, {
          fromField: "volume",
          toField: "volume"
        }],

        dataProvider: chartData,
        categoryField: "date"
      }],


      panels: [{
          showCategoryAxis: false,
          percentHeight: 70,

          valueAxes:[{
              id:"v1"
            }
          ],

          stockGraphs: [{
            id: "g1",
            valueField: "value",
            type: "line",
            lineThickness: 2,
            bullet: "none"
          }],

          stockLegend: {
            valueTextRegular: " ",
            markerType: "none"
          }
        }
      ],

      chartScrollbarSettings: {
        graph: "g1",
        usePeriod: "10mm",
        position: "top",
        updateOnReleaseOnly:false
      },

      chartCursorSettings: {
        valueBalloonsEnabled: true,
        valueLineBalloonEnabled:true,
        valueLineEnabled:true
      },

      panelsSettings: {
        usePrefixes: false
      }
    });
}

function buildAiAgentChart(chartData) {
  am4core.useTheme(am4themes_animated);
  var chart = am4core.create("stock-agent-graph", am4charts.XYChart);

  chart.data = chartData;


  // Create axes
  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  dateAxis.renderer.minGridDistance = 50;
  dateAxis.renderer.grid.template.location = 0.5;
  dateAxis.startLocation = 0.5;
  dateAxis.endLocation = 0.5;

  // Create value axis
  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

  // Create series
  var series1 = chart.series.push(new am4charts.LineSeries());
  series1.dataFields.valueY = "close";
  series1.dataFields.dateX = "date";
  series1.strokeWidth = 3;
  series1.tensionX = 0.8;
  series1.connect = false;

  var bullet = series1.bullets.push(new am4charts.CircleBullet());
  bullet.circle.fill = "bulletColor";

  chart.scrollbarX = new am4core.Scrollbar();
}
