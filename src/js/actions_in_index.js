function loadCompanyInfo(symbol, callback) {
  const url = "https://api.iextrading.com/1.0/stock/" + symbol + "/company";
  var request = new XMLHttpRequest();

  request.open('GET', url, true);
  request.onload = function () {

    // Begin accessing JSON data here
    data = JSON.parse(this.response)

    if (request.status >= 200 && request.status < 400) {
      callback(null, data);
    } else {
      console.log('error');
    }
  }

  request.send();
}


// Load stock graph with data of selected stock
function loadGraphData(symbol, callback) {
  const url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=3BXRR7PJ2AX1AMUN&outputsize=full&datatype=json";
  var request = new XMLHttpRequest();

  request.open('GET', url, true);
  request.onload = function () {

    // Begin accessing JSON data here
    data = JSON.parse(this.response)

    if (request.status >= 200 && request.status < 400) {
      callback(null, data);
    } else {
      console.log('error');
    }
  }

  request.send();
}


// Sets the nn performance graph into loading/waiting state
function agentWorking(episodes, episodeCount) {
  const agentWorkingDescription = document.querySelector(".trader-working #description");
  const traderWorking = document.querySelector(".performance-graph .trader-working");
  const agentPerformanceGraph = document.getElementById("stock-agent-graph");

  agentWorkingDescription.innerHTML = "The agent is on episode <span>" + String(episodes) + "/" + String(episodeCount) + "</span>"
  traderWorking.style.display = 'flex';

  // Hide no trader started
  const noTradingAgentText = document.getElementsByClassName("no-trader-started");
  for (var i = 0; i < noTradingAgentText.length; i++) {
    noTradingAgentText[i].style.display = "none";
  }
  noTradingAgentText.style.display = 'none';

  // Hide agent graph
  agentPerformanceGraph.style.display = "none";

  // Hide loading results
  const loadingDataWrapper = document.querySelector(".performance-graph .loading-results");
  loadingDataWrapper.style.display = "none";
}

function agentLoadingResults() {

  // Hide agent done element
  const traderDoneElement = document.querySelector(".performance-graph .agent-done");
  traderDoneElement.style.display = 'none';

  // Hide trader working
  const traderWorking = document.querySelector(".performance-graph .trader-working");
  traderWorking.style.display = 'none';

  // Hide no trader started
  const noTradingAgentText = document.getElementsByClassName("no-trader-started");
  for (var i = 0; i < noTradingAgentText.length; i++) {
    noTradingAgentText[i].style.display = "none";
  }
  // Show loading results
  const loadingDataWrapper = document.querySelector(".performance-graph .loading-results");
  loadingDataWrapper.style.display = "flex";
}

// The loading is done - hides the loading elements on screen
function agentDone() {
  const loadingWrapper = document.querySelector(".performance-graph .trader-working");
  const loadingDataWrapper = document.querySelector(".performance-graph .loading-results");

  // Hide loading wrapper
  loadingWrapper.style.display = "none";
  loadingDataWrapper.style.display = "none";
}

function loadAgentPerformaceData(symbol) {
  // Loading data animations on screen
  agentLoadingResults();

  symbol = symbol.id;

  const performanceDataLocation = firebase.database().ref('users/' + userIdTesting + '/trading_agents/' + symbol);

  // Get data from best episode
  performanceDataLocation.once('value').then(function(snap) {
    var actionsArray = snap.val()['performance'];
    console.log(actionsArray);

    // Update overall stats
    const profitText = document.querySelector(".profit .trading-interval-result");
    const growthText = document.querySelector(".growth .trading-interval-result");
    const flexWrapper = document.querySelector(".stock-nn-trader .flex-wrapper");
    profitText.innerHTML = Math.round(parseInt(snap.val()['total_profit'])) + "$";
    growthText.innerHTML = Math.round(parseInt(snap.val()['total_profit'])) + "%";
    flexWrapper.style.display = 'flex';

    console.log(symbol.toLowerCase());
    loadGraphData(symbol.toLowerCase(), function(error, res) {
      var chartData = [];

      for (var keyStock in res['Time Series (Daily)']) {
        for(var i = 0; i < actionsArray.length; i++) {
          if (keyStock === actionsArray[i]['date'] && parseInt(actionsArray[i]['action']) === 2) {
            var color = (parseInt(actionsArray[i]['sold_price']) - parseInt(actionsArray[i]['bought_price']) < 0)? "#e74c3c" : "#1abc9c";
            var newObj = {
              date: new Date(keyStock),
              close: parseInt(res['Time Series (Daily)'][keyStock]['4. close']),
              bulletColor: color
            }
            chartData.push(newObj)
          }
        }

        var color = "#fff";
        var newObj = {
          date: new Date(keyStock),
          close: parseInt(res['Time Series (Daily)'][keyStock]['4. close']),
          bulletColor: color
        }
        chartData.push(newObj)
      }

      chartData = chartData.reverse();
      console.log(chartData);

      // Show nn graph
      const traderGraph = document.getElementById("stock-agent-graph");
      traderGraph.style.display = 'block';

      // Data formating is done - remove loading from screen
      agentDone();

      // Build performance graph
      buildAiAgentChart(chartData);
    })
  })
}

function validateStatusResponse(snap) {
  console.log(snap.val());
  // If this is null then there is no agent started for this symbol
  if (snap.val() === null) {
    const hideElementsArray = [".performance-graph .trader-working", "#stock-agent-graph", ".stock-nn-trader .flex-wrapper"];
    const displayFlexElementsArray = [".stock-nn-trader .no-trader", ".no-trader-started"];

    // Hide all given in array
    for (var i = 0; i < hideElementsArray.length; i++) {
      const element = document.querySelector(hideElementsArray[i]);
      element.style.display = 'none';
    }

    // Set all given in array to display flex
    for (var i = 0; i < displayFlexElementsArray.length; i++) {
      const element = document.querySelector(displayFlexElementsArray[i]);
      element.style.display = 'flex';
    }

    const noTradingAgentText = document.getElementsByClassName("no-trader-started");
    for (var i = 0; i < noTradingAgentText.length; i++) {
      noTradingAgentText[i].style.display = "none";
    }

  } else {
    snap.forEach(function(child) {
      console.log(child.val());

      if (child.key == 'status') {
        var stat = child.val();
        var episodeCount = parseInt(stat['episode_count'])
        var episode = parseInt(stat['episode']);

        if (stat['status'] === 'processing') {
          agentWorking(episode, episodeCount)
        } else if (stat['status'] === 'done') {
          var newQuote = `
            <div class="no-trader-started agent-done">
              <p id="header">Trading Agent Done!</p>
              <p id="description">To view its performance click the button.</p>
              <button id="${snap.key}" class="shadow-button" onclick="loadAgentPerformaceData(${snap.key})">Load performance</button>
            </div>
          `

          var audio = new Audio('assets/sounds/notification_sound.mp3');
          audio.type = 'audio/mp3';
          audio.play();

          const performanceGraphWrapper = document.querySelector(".performance-graph");
          const hideElementsArray = [".performance-graph .trader-working", "#stock-agent-graph", ".stock-nn-trader .no-trader", ".stock-nn-trader .flex-wrapper", ".no-trader-started"];
          // Hide all given in array
          for (var i = 0; i < hideElementsArray.length; i++) {
            const element = document.querySelector(hideElementsArray[i]);
            element.style.display = 'none';
          }

          // Add get graph element
          performanceGraphWrapper.innerHTML += newQuote;
        }
      }
    });
  }
}


// Update the whole UI for a selected symbol from the Board
function updateUiTOSelectedQuote(symbol) {
  // Update company information
  var companyName = document.querySelector(".stock-company-info #company-name");
  var companyDescription = document.querySelector(".stock-company-info #company-description");
  var nnHeader = document.querySelector(".trading-stats .trader-ticker");

  // Hide trader working
  const traderWorking = document.querySelector(".performance-graph .trader-working");
  traderWorking.style.display = 'none';

  loadCompanyInfo(symbol.id.toLowerCase(), function(error, company) {
    companyName.innerHTML = company.companyName;
    companyDescription.innerHTML = company.description;
    nnHeader.innerHTML = company.symbol;
  })

  var graphElement = document.getElementById("stock-live-graph");
  var noGraphTextElement = document.querySelector(".no-graph");
  var loadingIcon = document.querySelector(".loading");

  loadingIcon.style.display = 'block';
  graphElement.style.display = 'none';
  noGraphTextElement.style.display = 'none';

  // Move loading animation down
  anime({
    targets: '.loading',
    top: '0',
    easing: 'easeInOutQuart',
    duration: 400
  });

  loadGraphData(symbol.id.toLowerCase(), function(error, data) {
    var chartData = [];

    for (var key in data['Time Series (Daily)']) {
      var newObj = {
        date: new Date(key),
        value: parseInt(data['Time Series (Daily)'][key]['4. close']),
        volume: parseInt(data['Time Series (Daily)'][key]['5. volume'])
      }
      chartData.push(newObj)
    }

    chartData = chartData.reverse();
    // Move loading animation down
    anime({
      targets: '.loading',
      top: '-100%',
      easing: 'easeInOutQuart',
      duration: 400
    });
    loadingIcon.style.display = 'none';
    graphElement.style.display = 'block';
    buildChart(chartData);
  })

  const tradingAgents = firebase.database().ref('users/' + userIdTesting + '/trading_agents/' + symbol.id);

  tradingAgents.once('value').then(function(snap) {
    validateStatusResponse(snap);
  });

}

const tradingAgents = firebase.database().ref('users/' + userIdTesting + '/trading_agents/');

tradingAgents.on('child_changed', function(snap) {
  validateStatusResponse(snap)
});
