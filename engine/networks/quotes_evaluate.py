from keras.models import load_model

from agent.agent import Agent
from net_functions import *
import sys

if len(sys.argv) != 3:
    print("Usage: python evaluate.py [stock] [model]")
    exit()

stock_symbol, model_name = sys.argv[1], sys.argv[2]
model = load_model("networks/models/" + model_name)
window_size = model.layers[0].input.shape.as_list()[1]

agent = Agent(window_size, True, model_name)
data = get_stock_data_vec(stock_symbol)
l = len(data) - 1
batch_size = 32

state = get_state(data, 0, window_size + 1)
total_profit = 0
agent.inventory = []

for t in range(l):
    action = agent.act(state)

    next_state = get_state(data, t + 1, window_size + 1)
    reward = 0

    if action == 1:  # Buy
        agent.inventory.append(data[t])
        print("Buy: " + format_price(data[t]))

    elif action == 2 and len(agent.inventory) > 0:  # Sell
        bought_price = agent.inventory.pop(0)
        reward = max(data[t] - bought_price, 0)

        total_profit += data[t] - bought_price
        print("Sell: " + format_price(data[t]) + " | Profit: " + format_price(data[t] - bought_price))

    done = True if t == l - 1 else False
    agent.memory.append((state, action, reward, next_state, done))
    state = next_state

    if done:
        print("--------------------------------")
        print(stock_symbol + " Total Profit: " + format_price(total_profit))
        print("--------------------------------")
