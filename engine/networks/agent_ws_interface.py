from flask import Flask, render_template, redirect, url_for,request
from flask import make_response
from agent.agent import Agent
import sys
from net_functions import *
from threading import Thread
import pyrebase
from keras import backend as K

# Initialize socket.io for python
app = Flask(__name__)


def agentThread(stock_symbol, window_size, episode_count):
    # Starts a fesh keras session
    K.clear_session()

    # Results in memory
    profits = []
    trading_history = []

    # Initialize python firebase database
    config = {
        "apiKey": "AIzaSyBgDnlmtggLJewCIRBN2o8Yg1oc7NGilIE",
        "authDomain": "unity-trader.firebaseapp.com",
        "databaseURL": "https://unity-trader.firebaseio.com",
        "projectId": "unity-trader",
        "storageBucket": "",
        "messagingSenderId": "84245183683"
    }

    firebase = pyrebase.initialize_app(config)
    db = firebase.database()

    # Initialize agent and data
    agent = Agent(window_size, False, "model_ep110")
    dates, data = get_stock_data_vec(stock_symbol)
    l = len(data) - 1
    batch_size = 32  # Runs in one batch


    for e in range(episode_count):  # Expects the episode count to have staring index of 1
        print("Episode " + str(e + 1) + "/" + str(episode_count))

        db.child('users/334gfbbge4556/trading_agents/' + stock_symbol + '/').child('status').set({
            'status': 'processing',
            'episode': str(e + 1),
            'episode_count': str(episode_count)
        })

        wallet_size = 0;

        sub_trading_history = []

        # Get the current state - between the time points
        state = get_state(data, 0, window_size + 1)  # First state, ergo: t=0

        total_profit = 0
        agent.inventory = []

        for t in range(l):
            # Agent returns an action; buy, hold, sell
            action = agent.act(state)

            # What's the next state?
            next_state = get_state(data, t + 1, window_size + 1)  # Looping over
            reward = 0

            if action == 1:  # Buy
                agent.inventory.append(data[t])  # append closing price to inventory
                # print("Buy: " + format_price(data[t]))
                wallet_size -= data[t]
                sub_trading_history.append({"date": dates[t], "action": 1, "bought_price": data[t]})
                # await sio.emit('agent_response', tradingHistory, room=sid)

            elif action == 2 and len(agent.inventory) > 0:  # Sell
                bought_price = agent.inventory.pop(0)
                reward = max(data[t] - bought_price, 0)
                total_profit += data[t] - bought_price
                wallet_size += data[t]
                sub_trading_history.append({"date": dates[t], "action": 2, "bought_price": bought_price, "sold_price": data[t]})
                # await sio.emit('agent_response', tradingHistory, room=sid)

                # print("Sell: " + format_price(data[t]) + " | Profit: " + format_price(data[t] - bought_price))

            done = True if t == l - 1 else False
            agent.memory.append((state, action, reward, next_state, done))
            state = next_state

            if done:
                print("--------------------------------")
                print("Total Profit: " + format_price(total_profit))
                print("--------------------------------")
                profits.append(total_profit);
                trading_history.append(sub_trading_history);

            if len(agent.memory) > batch_size:
                agent.exp_replay(batch_size)

        if e % 10 == 0:
            agent.model.save("/home/patrick/Desktop/models/model_ep" + str(e))

    np_profits = np.array([profits])
    largest_profit = np.argmax(np_profits)

    db.child('users/334gfbbge4556/trading_agents/' + stock_symbol + '/').set({
        'status': {
            'status': 'done',
            'episode_count': str(episode_count)
        },
        'total_profit': profits[largest_profit],
        'performance': trading_history[largest_profit]
    });


@app.route('/start_agent', methods=['GET', 'POST'])
def message():
    '''
        Generate a background thread id
        Send that id to firebase
        In background thread start a websocket that is on the given id.
    '''
    symbol = request.args.get('symbol')
    windows = request.args.get('windows');
    episodes = request.args.get('episodes');
    agent_thread = Thread(target=agentThread, args=(symbol, int(windows), int(episodes),))
    agent_thread.start()
    return(symbol)



if __name__ == "__main__":
    app.run(host='localhost', port = 5005, debug = True)
