import numpy as np
import math
import urllib.request


def format_price(n):
    return ("-$" if n < 0 else "$") + "{0:.2f}".format(abs(n))


# returns the vector containing stock data from a fixed file.
def get_stock_data_vec(key):

    # Get historical stock prices from alphavantage
    url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + key + "&apikey=3BXRR7PJ2AX1AMUN&outputsize=full&datatype=csv"
    urllib.request.urlretrieve(url, "stock_data_file.csv")

    vec = []
    dates = []
    lines = open("stock_data_file.csv").read().splitlines()

    for line in lines[1:]:
        # Appends close price to vec
        vec.append(float(line.split(",")[4]))
        dates.append(line.split(",")[0])

    dates.reverse()
    vec.reverse()

    return (dates, vec)



# returns the sigmoid
def sigmoid(x):
    return 1 / (1 + math.exp(-x))


# returns an n-day state representation
def get_state(data, t, n):
    d = t - n + 1  # Time frame
    block = data[d:t + 1] if d >= 0 else -d * [data[0]] + data[0:t + 1]  # else: n start is 0 running going to t
    res = []

    for i in range(n - 1):  # In range n - 1; since the index is shifted 1
        res.append(sigmoid(block[i + 1] - block[i]))

    return np.array([res])
