from agent.agent import Agent
import sys
from net_functions import *

if len(sys.argv) != 4:
    print("Usage: python train.py [stock] [window] [episodes]")
    exit()

# Variables from input
stock_symbol, window_size, episode_count = sys.argv[1], int(sys.argv[2]), int(sys.argv[3])

# Initialize agent and data
agent = Agent(window_size, False, "model_ep110")
data = get_stock_data_vec(stock_symbol)
l = len(data) - 1
batch_size = 32  # Runs in one batch

for e in range(episode_count + 1):  # Expects the episode count to have staring index of 0
    print("Episode " + str(e) + "/" + str(episode_count))

    # Get the current state - between the time points
    state = get_state(data, 0, window_size + 1)  # First state, ergo: t=0

    total_profit = 0
    agent.inventory = []

    for t in range(l):
        # Agent returns an action; buy, hold, sell
        action = agent.act(state)

        # What's the next state?
        next_state = get_state(data, t + 1, window_size + 1)  # Looping over
        reward = 0

        if action == 1:  # Buy
            agent.inventory.append(data[t])  # append closing price to inventory
            print("Buy: " + format_price(data[t]))

        elif action == 2 and len(agent.inventory) > 0:  # Sell
            bought_price = agent.inventory.pop(0)
            reward = max(data[t] - bought_price, 0)
            total_profit += data[t] - bought_price

            print("Sell: " + format_price(data[t]) + " | Profit: " + format_price(data[t] - bought_price))

        done = True if t == l - 1 else False
        agent.memory.append((state, action, reward, next_state, done))
        state = next_state

        if done:
            print("--------------------------------")
            print("Total Profit: " + format_price(total_profit))
            print("--------------------------------")

        if len(agent.memory) > batch_size:
            agent.exp_replay(batch_size)

    if e % 10 == 0:
        agent.model.save("/home/patrick/Desktop/models/model_ep" + str(e))
