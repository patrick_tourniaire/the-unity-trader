import requests
import json


with open('../credentials/api_data.json') as file:
    api_credentials = json.load(file)

# Get stock data in block format from NODE server
def get_block_data(params):
    apiKey = api_credentials['aplhavantage']['key']
    url = "http://localhost:8081/api/get_quote"
    params['apikey'] = apiKey

    r = requests.get(url=url, params=params)
    data = r.json()