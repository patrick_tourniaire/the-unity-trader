﻿

# The Unity Trader - Source Code

The Unity Trader is a project I created to make stock analysis a quicker and easier process for beginner traders. The Unity Trader is in no way a bulletproof solution, but it does give a quick and effective way of analyzing how the market is doing. All of this is achieved by utilizing a **Q-learning type neural network** trading on previous market history, but it could also be setup to give recommendations on stocks.


# Project Structure
   ```
    ├── .idea                   
	├── engine                  # The engine running the Q-learning nn and websocket connections.
	├── js                      # NODE.JS backend for retrieving stock data.
	├── node_modules            # All included modules for the front- and back-end. 
	├── src                     # Front-end running for main site
		├── assets						# Assets including css, images and vectors
		├── js							# Front-end javascript files
		├── pages						# All html pages without index.html
		├── index.html					# Main site
	├── venv                    # Virtual enviroment for python built for the python engine
	└── the-unity-trader.xd     # Design file for website
```


# Assessment Requirements

|Requirement|  Location in code|
|--|--|
|Vector graphics | [eg. src/index.html line 154] |
|Audio |Programmatically implemented [eg. src/js/actions_in_index.js line 196 - 198]|
|Functions| [eg. Everywhere]| 
|Loops| For-loops [eg. Everywhere] While-loops [eg. src/js/animations/action_animations.js line 97 - 103]|
|Variables| [eg. Everywhere]|
|Images| src/pages/about.html|
