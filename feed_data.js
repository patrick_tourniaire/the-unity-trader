
const tokens = require("../js/credentials/api_data.json");
const cron = require('node-cron');
const retrieve_quotes = require("../js/get_data/retrieve_quotes.js");


// HACK: Removes numerals from the json child names.
function cleanRawData(data) {
  var quote = data['Global Quote'];
  var newData = {};

  for (var child in quote) {
    // Remove the numeral from key name
    var noNumeral = child.split(" ").slice(1);
    var newName = noNumeral.join("_");

    newData[newName] = quote[child];
  }

  return newData;
}

cron.schedule('*/5 * * * * *', () => {
  var retreveQuotes = new retrieve_quotes({
    function: 'GLOBAL_QUOTE',
    symbol: 'MSFT',
    apikey: tokens['aplhavantage']['key']});

  retreveQuotes.getCurrentQuote(function(error, data) {
    if (error) { console.error(error); }
    console.log(cleanRawData(data));
  });
})
