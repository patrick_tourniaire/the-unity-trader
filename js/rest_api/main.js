const express = require("express");
const http = require('http').Server(express)
const io = require('socket.io')(http)

const retrieve_quotes = require("../get_data/retrieve_quotes.js");
const retrieve_news = require("../get_data/retrieve_news.js");
const tokens = require("../../credentials/api_data.json");


// REST API settings
var app = express();
var port = process.env.PORT || 8081;

function cleanRawData(data) {
  var quote = data['Global Quote'];
  var newData = {};

  for (var child in quote) {
    // Remove the numeral from key name
    var noNumeral = child.split(" ").slice(1);
    var newName = noNumeral.join("_");

    newData[newName] = quote[child];
  }

  return newData;
}

function apiParameterFilter(accParams, keyType, req) {
  var filteredParams = [];
  if (req.query['apikey'] === tokens[keyType]['key']) {
    for (var paramName in req.query) {
      if (accParams.includes(paramName)) {
        filteredParams.push(paramName);
      }
    }
  }

  return filteredParams
}

// Reformat the quotes to block format for the rnn.
function qLearningBlockFormat(data) {
  var newData = {};
  for (var point in data) {
    newData[point] = data[point]["4. close"]
  }

  return newData;
}

// Get quotes from server
app.get('/api/get_quote', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  var accepted_params = ["function", "symbol", "interval", "outputsize", "datatype", "is_block", "apikey"];
  var mandatory_params = ["function", "symbol", "interval", "apikey"]; // Indecies of accepted_params

  var filteredParams = apiParameterFilter(accepted_params, "aplhavantage", req);

  if (filteredParams.some(i => mandatory_params.includes(i))) {
    var dataObject = {};
    for (var paramName in req.query) {
      dataObject[paramName] = req.query[paramName];
    }

    var retrieveQuotes = new retrieve_quotes(dataObject);
    retrieveQuotes.getCurrentQuote(function(error, data) {
      res.send({
        "error": (error ? true : false),
        "response": (req.query["is_block"] == 'true' ? qLearningBlockFormat(data['Time Series ('+req.query['interval']+')']) : data)
      });
    })
  } else {
    res.send({
      "error": "true",
      "response": "The parameters you entered does not match the mandatory parameters: " + mandatory_params
    });
  }

});


// Get twitter news data
app.get('/api/get_twitter_news', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  var accepted_params = [
    "q", "result_type",
    "geocode", "lang",
    "locale", "count",
    "until", "since_id",
    "since_id", "include_entities",
    "compact", "apikey"];
  var mandatory_params = ["q", "compact", "apikey"];

  var filteredParams = apiParameterFilter(accepted_params, "unity_trader", req);

  if (filteredParams.some(i => mandatory_params.includes(i))) {
    var dataObject = {};
    for (var paramName in req.query) {
      dataObject[paramName] = req.query[paramName];
    }

    var retrieveNews = new retrieve_news(dataObject);
    retrieveNews.getTweets(function(error, data) {
      res.send({
        "error": (error ? true : false),
        "response": (req.query['compact'] === 'true' && !error ? retrieveNews.compactTwitterData(data) : data)
      });

      retrieveNews.buildSentimentReport(retrieveNews.compactTwitterData(data))
    })
  } else {
    res.send({
      "error": "true",
      "response": "The parameters you entered does not match the mandatory parameters: " + mandatory_params
    });
  }
})

// start the server
app.listen(port);
console.log('Server started! At http://localhost:' + port);
