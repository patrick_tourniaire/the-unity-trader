const OAuth = require("oauth");
const tokens = require("../../credentials/api_data.json");
const sentiment = require("sentiment");

class retrieveNews {

  constructor(params) {
    this.params = params;

    // Authenticate to Twitter api
    this.oauth = new OAuth.OAuth(
      'https://api.twitter.com/oauth/request_token',
      'https://api.twitter.com/oauth/access_token',
      tokens['twitter']['consumer_token'],
      tokens['twitter']['consumer_secret'],
      '1.0A',
      null,
      'HMAC-SHA1'
    );
  }

  buildUrl(options, baseUrl) {
    var num_options = 0;
    for (var option in options) {
      num_options += 1;
      baseUrl += option.toString() + "=" + options[option].toString() + (num_options < Object.keys(options).length ? "&" : "");
    }
    return baseUrl;
  }

  getTweets(callback) {
    var url = this.buildUrl(this.params, 'https://api.twitter.com/1.1/search/tweets.json?');

    this.oauth.get(url,
      tokens['twitter']['access_token'],
      tokens['twitter']['access_secret'],
      function(error, data, response) {
        if (error) {callback(true, error)}
          data = JSON.parse(data);
          callback(null, data);
      })
  }

  compactTwitterData(data,
    tweet_data_points = ["created_at", "text", "retweet_count"],
    user_data_points = ["verified", "followers_count"]) {

    var newData = [];
    for (var n = 0; n < data['statuses'].length - 1; n++) {
      var d = data['statuses'][n];
      var newEntry = {};

      for (var i = 0; i < tweet_data_points.length; i++) {
        newEntry[tweet_data_points[i]] = d[tweet_data_points[i]];
      }
      for (var i = 0; i < user_data_points.length; i++) {
        newEntry[user_data_points[i]] = d["user"][user_data_points[i]];
      }

      newData.push(newEntry);
    }

    return newData;
  }
}

module.exports = retrieveNews;
