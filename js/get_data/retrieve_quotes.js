const https = require("https");
const tokens = require("../../credentials/api_data.json");
const term = require( 'terminal-kit' ).terminal ;
const Sentiment = require('sentiment');

class streamQuotes {

  constructor(params) {
    this.params = params;
  }

  buildUrl(options, baseUrl) {
    var num_options = 0;
    for (var option in options) {
      num_options += 1;
      baseUrl += option.toString() + "=" + options[option].toString() + (num_options < Object.keys(options).length ? "&" : "");
    }
    return baseUrl;
  }

  getCurrentQuote(callback) {
    var url = this.buildUrl(this.params, 'https://www.alphavantage.co/query?');
    https.get(url, (res) => {
      term.bgBlack.brightGreen('[statusCode]:')
      term.bold(" " + res.statusCode + '\n\n');
      var dataBody = '';

      res.on('data', (d) => {
        if (d != null) {
          term.bgBlack.brightGreen('[SUCCESS]');
          term.bold(" Retrieved data from the requested url: " + url + '\n\n');
          dataBody += d;
        } else {
          term.bgBlack.brightGreen('[UNEXPECTED ERROR]');
          term.bold(" It looks like the retrieved data is empty...\n\n");
          callback(404, null);
        }
      });

      res.on('end', function() {
        var response = JSON.parse(dataBody);
        term.bgBlack.brightGreen('[SUCCESS] Loaded data into the correct format: JSON');
        term.bgDefaultColor('\n\n');
        callback(null, response);
      })

    }).on('error', (e) => {
      callback(e, null);
    });
  }

  buildSentimentReport(data) {
    var list = data;

    var pos = 0;
    var neg = 0;
    var size = list.length;

    for (var i = 0; i < data.length; i++) {
      var tweetBlock = data[i];
      var text = tweetBlock['text'];

      var sentiment = new Sentiment();
      var result = sentiment.analyze(text);
      console.log(result);
    }
  }
}

module.exports = streamQuotes;
