const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const tokens = require('../credentials/api_data.json');
const retrieve_quotes = require("../js/get_data/retrieve_quotes.js");
const term = require( 'terminal-kit' ).terminal;

// HACK: Removes numerals from the json child names.
function cleanRawData(data) {
  var quote = data['Global Quote'];
  var newData = {};

  for (var child in quote) {
    // Remove the numeral from key name
    var noNumeral = child.split(" ").slice(1);
    var newName = noNumeral.join("_");

    newData[newName] = quote[child];
  }

  return newData;
}


io.on('connection', function(socket) {
  /*

    Get a quote request in terms of an Object.
    For n-seconds get Global Quote from the alphavantage api.
    Send it back as a formated Object.

  */

  term.bgBlack.brightGreen('[CONNECTION]');
  term.bold(" A client has connected to the server!\n\n");

  // Wait for an Array of needed quotes.
  socket.on('current_quotes_form', function(quote) {
    /*
      What needs to be sent:
        Quote ticker.
    */
    var dataResponse = [];
    
    var retreiveQuotes = new retrieve_quotes({
      function: 'GLOBAL_QUOTE',
      symbol: quote,
      apikey: tokens['aplhavantage']['key']});

    retreiveQuotes.getCurrentQuote(function(error, data) {
      if (error) { console.error(error); }
      io.emit('current_quotes', cleanRawData(data));
    })
  })

  socket.on('disconnect', function() {
    term.bgBlack.brightRed('[DISCONNECT]');
    term.bold(" A client has disconnected from the server!\n\n");
  })

})

http.listen(3000, function(){
  console.log('listening on *:3000');
});
